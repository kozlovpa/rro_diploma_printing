import csv





def initialize_xml(keys):
    # read xml template to get beginning and ending of the xml file
    # template can be getted in adobe illustrator by adding variables into working space

    file_beginning = [
        '<?xml version="1.0" encoding="utf-8"?>\n',
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20001102//EN"    ' +
                '"http://www.w3.org/TR/2000/CR-SVG-20001102/DTD/svg-20001102.dtd" [\n',
        '\t<!ENTITY ns_graphs "http://ns.adobe.com/Graphs/1.0/">\n',
        '\t<!ENTITY ns_vars "http://ns.adobe.com/Variables/1.0/">\n',
        '\t<!ENTITY ns_imrep "http://ns.adobe.com/ImageReplacement/1.0/">\n',
        '\t<!ENTITY ns_custom "http://ns.adobe.com/GenericCustomNamespace/1.0/">\n',
        '\t<!ENTITY ns_flows "http://ns.adobe.com/Flows/1.0/">\n',
        '\t<!ENTITY ns_extend "http://ns.adobe.com/Extensibility/1.0/">\n',
        ']>\n',
        '<svg>\n',
        '<variableSets  xmlns="&ns_vars;">\n',
        '\t<variableSet  locked="none" varSetName="binding1">\n',
        '\t\t<variables>\n']

    for key in keys:
        file_beginning += [
            '\t\t\t<variable  category="&ns_flows;" trait="textcontent" varName="' + str(key) + '"></variable>\n'
            ]

    file_beginning += [
        '\t\t</variables>\n',
        '\t\t<v:sampleDataSets  xmlns="&ns_custom;" xmlns:v="&ns_vars;">\n'
    ]

    file_ending = [
        '\t\t</v:sampleDataSets>\n',
        '\t</variableSet>\n',
        '</variableSets>\n',
        '</svg>\n']
    return file_beginning, file_ending


def read_csv(file_name):
    # parse csv file
    csv.register_dialect('myDialect', delimiter = ';')
    table = []
    with open(csv_name, 'r') as file_name:
        reader = csv.DictReader(file_name, dialect='myDialect')
        for row in reader:
            row_dict = dict(row)
            table_keys = list(row_dict.keys())

            for key in table_keys:
                row_dict[key.lstrip().rstrip().strip()] = row_dict.pop(key)
                t_key = key.lstrip().rstrip().strip()

            row_dict.pop(' ', None)
            row_dict.pop('', None)
            row_dict.pop('\ufeff', None)

            table.append(row_dict)

    print("table len: %i, table keys: %s" % (len(table), list(table[0].keys())))
    return table, list(table[0].keys())


def create_sampleDataSet(keys, value, label=None, split_names=0, max_len=65):
    sample = []
    label = value.get(keys[0]) if label is None else label
    sample.append("\t\t\t<v:sampleDataSet dataSetName=\"" + label + "\">\n")
    for key in keys:
        sample.append("\t\t\t\t<" + str(key) + ">\n")
        t_value = value.get(key) if ((value is not None) and (value.get(key) is not None)) else " "
        if key.lower() == "name":
        #     split name
            for _ in range(split_names):
                space = t_value.find(" ")
                sample.append("\t\t\t\t\t <p>" + t_value[:space] + "</p>\n")
                t_value = t_value[space + 1:]
        elif len(t_value) > max_len:
        #     split string into several
            while len(t_value) > max_len:
                space = t_value.rindex(" ", 0, max_len)
                sample.append("\t\t\t\t\t <p>" + t_value[:space] + "</p>\n")
                t_value = t_value[space + 1:]

        sample.append("\t\t\t\t\t <p>" + t_value + "</p>\n")
        sample.append("\t\t\t\t</" + str(key) + ">\n")

    sample.append("\t\t\t</v:sampleDataSet>\n")
    return sample

def generate_entity(table, keys, extra_key=None, a_place = None, split_names=0, max_len=65, number_of_entities=1):
    # extra_key = [key, value]

    middle_part = []
    cnt = 0
    t_keys = keys.copy()
    if a_place is not None:
        t_keys.append('place')

    for row in table:
        if (extra_key is None) or (row.get(extra_key[0]) == extra_key[1]):
    #         we should generate and entity
            cnt += 1
            label = (row.get(t_keys[0]) if extra_key is None else extra_key[1]) + " " + str(cnt)
            if a_place is not None:
                row.update({'place':a_place})
            middle_part += create_sampleDataSet(t_keys,row,label=label,split_names=split_names,max_len=max_len)

    for _ in range(number_of_entities-cnt):
        label = (row.get(t_keys[0]) if extra_key is None else extra_key[1]) + " " + str(cnt+_+1)
        middle_part += create_sampleDataSet(t_keys, None, label=label, split_names=split_names, max_len=max_len)

    return middle_part

def convert_places(i_places, length=-1):
    str_places = []
    for i in i_places:
        t_str = "за " + str(i) + " место"
        str_places.append(t_str)
    if len(i_places) < length:
        for i in range(length-len(i_places)):
            str_places.append(str_places[-1])
    return str_places

def generate_xml(table, keys, entities_keys=None, extra_key='code',places = None, split_names=0, max_len=65, number_of_entities=1):
    # convert csv file into xml code
    # it provides a possibillity to automatically addition
    # all information into ai file
    file_middle = []

    if places is not None:
        places = convert_places(places, length=len(entities_keys))

    if entities_keys is not None:
        for i in range(len(entities_keys)):
            ent_key = entities_keys[i]
            t_key = places[i] if places is not None else None
            file_middle +=generate_entity(table, keys, extra_key=[extra_key,ent_key],split_names=split_names, max_len=max_len,
                            number_of_entities=number_of_entities, a_place=t_key)
            # generate sampleDataSet entities for current ent_key
            pass
    else:
        file_middle = generate_entity(table,keys,split_names=split_names,max_len=max_len,number_of_entities=number_of_entities)
        # generate sampleDataSet for all table
        pass

    return file_middle





if __name__ == '__main__':

    csv_name = "table.csv"
    xml_name = "variables.xml"
    commands_name = "commands.txt"

    xml_outputname = "out/third_place.xml"
    max_len = 58

    is_diploma = True

    data,table_keys = read_csv(csv_name)
    file_beginning = []
    file_middle = []
    file_ending = []

    if is_diploma:

        requested_keys = ['stream', 'theme', 'name']
        file_beginning, file_ending = initialize_xml(requested_keys + ["place"])

        commands = []
        with open(commands_name, encoding='utf-8') as file:
            commands = [line.strip() for line in file.readlines()]
        category = commands[0][:commands[0].find("-")]

        print("There're %s commands in %s cathegory rewarded" % (len(commands), category))

        file_middle = generate_xml(data, requested_keys, commands, 'code', [1, 2])
    else:
        file_beginning, file_ending = initialize_xml(table_keys)

        file_middle = generate_xml(data, table_keys)



    full_file = file_beginning + file_middle + file_ending
    outfile = open(xml_outputname, "w", encoding='utf-8')
    outfile.writelines(full_file)
    outfile.close()

    print("File \"%s\" succsessfully created" % xml_outputname)