import csv


max_team_value = 3
with_empties = True
csv_name = "data_new.csv"
xml_name = "variables.xml"
commands_name = "commands.txt"



data = []
# parse csv file
csv.register_dialect('myDialect', delimiter = ';')
with open(csv_name, 'r') as csvfile:
    reader = csv.DictReader(csvfile, dialect='myDialect')
    for row in reader:
        # print(dict(row))
        data.append(dict(row))

table_keys = list(data[0].keys())
# table_keys.remove('')
print("table len: %i, table keys: %s" % (len(data), table_keys))



# read xml template to get beginning and ending of the xml file
# template can be getted in adobe illustrator by adding variables into working space
file_xml = []
with open(xml_name, encoding='utf-8') as xmlfile:
    file_xml = xmlfile.readlines()

file_beginning = []
for string in file_xml:
    if "sampleDataSets" in string:
        # print("True  | " + string)
        file_beginning.append(string)
        break
    else:
        # print("False | " + string)
        file_beginning.append(string)

file_ending = file_xml[-4:]
print("file xml len %s and beginning len %s" %(len(file_xml), len(file_beginning)))

tabs = file_beginning[-1][:file_beginning[-1].find("<v:sampleDataSets")] + '\t'



commands = []
with open(commands_name, encoding='utf-8') as file:
    commands = [line.strip() for line in file.readlines()]
category = commands[0][:commands[0].find("-")]

print("There're %s commands in %s cathegory rewarded" % (len(commands), category))


category_reward = ""
if commands[0].find("\"") != -1:
    category_reward = commands[0][commands[0].find("\""):]
    print("There's special reward for open category: %s" % category_reward)
    commands[0] = commands[0][:commands[0].find("\"")-1]

# convert csv file into xml code
# it provides a possibillity to automatically addition
# all information into ai file
file_middle = []
file_middle_coach = []
index = 1
index_coach = 1
for i_command in range(len(commands)):
    command = commands[i_command]
    print("%i place: %s" %((i_command+1),command))
    team = []
    trainers = []
    for i in data:
        if (i.get('code') == command):
            if (i.get('type') != "тренер"):
                team.append(i)
            else:
                trainers.append(i)

    rro_keys = ['surname', 'name', 'training_center', 'region', 'category']

    for j in range(len(team)):
        i_key = str(index) + " " + command + " " + str(j)
        index += 1
        file_middle.append(tabs + "<v:sampleDataSet dataSetName=\"" + i_key + "\">\n")


        for key in rro_keys:
            if key == 'category' and str(team[j].get(key)).find('\n') != -1:
                if len(category_reward) < 1:
                    file_middle.append(tabs + "\t<" + str(key) + ">\n")
                    str_category = str(team[j].get(key)).split('\n')
                    file_middle.append(tabs + "\t\t <p>" + str_category[0] + "</p>\n")

                    file_middle.append(tabs + "\t\t <p>" + str_category[1] + "</p>\n")
                    file_middle.append(tabs + "\t</" + str(key) + ">\n")
                else:
                    file_middle.append(tabs + "\t<" + str(key) + ">\n")
                    str_category = str(team[j].get(key)).split('\n')
                    file_middle.append(tabs + "\t\t <p>" + category_reward + "</p>\n")
                    file_middle.append(tabs + "\t</" + str(key) + ">\n")
            elif key == 'training_center' and len(str(team[j].get(key))) > 63:
                file_middle.append(tabs + "\t<" + str(key) + ">\n")
                str_training_center = str(team[j].get(key))
                space = str_training_center.rindex(" ", 0, 63)
                str_training_center = [str_training_center[:space], str_training_center[space+1:]]

                file_middle.append(tabs + "\t\t <p>" + str_training_center[0] + "</p>\n")
                file_middle.append(tabs + "\t\t <p>" + str_training_center[1] + "</p>\n")
                file_middle.append(tabs + "\t</" + str(key) + ">\n")
            else:
                file_middle.append(tabs + "\t<" + str(key) + ">\n")
                file_middle.append(tabs + "\t\t <p>" + str(team[j].get(key)) + "</p>\n")
                file_middle.append(tabs + "\t</" + str(key) + ">\n")

        file_middle.append(tabs + "\t<place>\n")
        file_middle.append(tabs + "\t\t <p>за " + str(i_command+1) + " место</p>\n")
        file_middle.append(tabs + "\t</place>\n")

        file_middle.append(tabs + "</v:sampleDataSet>\n")




    for j in range(len(trainers)):
        i_key = str(index_coach) + " " + command + " " + str(j)
        index_coach += 1
        file_middle_coach.append(tabs + "<v:sampleDataSet dataSetName=\"" + i_key + "\">\n")


        for key in rro_keys:
            if key == 'category' and str(trainers[j].get(key)).find('\n') != -1:
                file_middle_coach.append(tabs + "\t<" + str(key) + ">\n")
                str_category = str(trainers[j].get(key)).split('\n')
                file_middle_coach.append(tabs + "\t\t <p>" + str_category[0] + "</p>\n")

                file_middle_coach.append(tabs + "\t\t <p>" + str_category[1] + "</p>\n")
                file_middle_coach.append(tabs + "\t</" + str(key) + ">\n")
            elif key == 'training_center' and len(str(trainers[j].get(key))) > 65:
                file_middle_coach.append(tabs + "\t<" + str(key) + ">\n")
                str_training_center = str(trainers[j].get(key))
                space = str_training_center.rindex(" ", 0, 65)
                str_training_center = [str_training_center[:space], str_training_center[space+1:]]

                file_middle_coach.append(tabs + "\t\t <p>" + str_training_center[0] + "</p>\n")
                file_middle_coach.append(tabs + "\t\t <p>" + str_training_center[1] + "</p>\n")
                file_middle_coach.append(tabs + "\t</" + str(key) + ">\n")
            else:
                file_middle_coach.append(tabs + "\t<" + str(key) + ">\n")
                file_middle_coach.append(tabs + "\t\t <p>" + str(trainers[j].get(key)) + "</p>\n")
                file_middle_coach.append(tabs + "\t</" + str(key) + ">\n")

        file_middle_coach.append(tabs + "\t<place>\n")
        if i_command == 0:
            file_middle_coach.append(tabs + "\t\t <p>за подготовку команды победителей</p>\n")
        else:
            file_middle_coach.append(tabs + "\t\t <p>за подготовку команды призёров (" + str(i_command+1) + " место)</p>\n")
        file_middle_coach.append(tabs + "\t</place>\n")

        file_middle_coach.append(tabs + "</v:sampleDataSet>\n")




    if with_empties:
        for j in range(len(team), max_team_value):
            i_key = str(index)  + " " + command + " " + str(j)
            index += 1
            # print("extra j: %s" %(i_key))
            file_middle.append(tabs + "<v:sampleDataSet dataSetName=\"" + i_key + "\">\n")

            for key in rro_keys:
                file_middle.append(tabs + "\t<" + str(key) + ">\n")
                file_middle.append(tabs + "\t\t <p> </p>\n")
                file_middle.append(tabs + "\t</" + str(key) + ">\n")

            file_middle.append(tabs + "\t<place>\n")
            file_middle.append(tabs + "\t\t <p> </p>\n")
            file_middle.append(tabs + "\t</place>\n")

            file_middle.append(tabs + "</v:sampleDataSet>\n")




xml_ouputname = "out/" + category + "_members_" + category_reward[1:-1] + "_" + xml_name
full_file = file_beginning + file_middle + file_ending
outfile = open(xml_ouputname,"w", encoding='utf-8')
outfile.writelines(full_file)
outfile.close()

print("File \"%s\" succsessfully created" %xml_ouputname)

xml_ouputname = "out/" + category  + "_trainers_" + category_reward[1:-1] + "_" + xml_name
full_file = file_beginning + file_middle_coach + file_ending
outfile = open(xml_ouputname,"w", encoding='utf-8')
outfile.writelines(full_file)
outfile.close()

print("File \"%s\" succsessfully created" %xml_ouputname)