from PyPDF2 import PdfFileReader
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF


def svd_test():
    drawing = svg2rlg("diploma2.svg")
    renderPDF.drawToFile(drawing, "diploma2_out.pdf")
    # cairosvg.svg2pdf(url='diploma2.svg', write_to='diploma2_out2.pdf')


def main():
    pdf = PdfFileReader("diploma3.pdf")
    print(pdf.getNumPages())
    first_page = pdf.getPage(0)
    print(first_page.extractText())

def another_test():
    from PyPDF2 import PdfFileReader, PdfFileWriter

    replacements = [
        ("name", "demo")
    ]

    pdf = PdfFileReader(open("diploma3.pdf", "rb"))
    writer = PdfFileWriter()

    for page in pdf.pages:
        contents = page.getContents().getData()
        for (a, b) in replacements:
            contents = contents.replace(a.encode('utf-8'), b.encode('utf-8'))
        page.getContents().setData(contents)
        writer.addPage(page)

    with open("diploma_modified.pdf", "wb") as f:
        writer.write(f)

if __name__ == '__main__':
    # main()
    another_test()
    # svd_test()