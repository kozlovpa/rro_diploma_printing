import csv

# csv_name = "t2c/sertificates.csv"
# csv_name = "pobed.csv"
# csv_name = "stem_olymp_2021.csv"
# csv_name = "blags.csv"
csv_name = "blags-extra.csv"
# csv_name = "rost/special_diplomas.csv"
# xml_name = "project_school/variables.xml"
# xml_ouputname = "project_school/out_"  + xml_name[ xml_name.find("/")+1 :]
xml_outputname = "blags-extra.xml"
# max_len = 65
max_len = 60
# max_len = 55
# max_len = 35
split_names = False
t_max_len = 100
split_by_n = True



def read_csv(file_name):
    # parse csv file
    # csv.register_dialect('myDialect', delimiter = ';')
    csv.register_dialect('myDialect', delimiter = ',')
    table = []
    with open(csv_name, 'r', encoding='utf-8') as file_name:
    # with open(csv_name, 'r', encoding='windows-1251') as file_name:
        reader = csv.DictReader(file_name, dialect='myDialect')
        for row in reader:
            row_dict = dict(row)
            table_keys = list(row_dict.keys())

            for key in table_keys:
                row_dict[key.lstrip().rstrip().strip()] = row_dict.pop(key)
                t_key = key.lstrip().rstrip().strip()

            row_dict.pop(' ', None)
            row_dict.pop('', None)
            row_dict.pop('\ufeff', None)
            row_dict.pop('\ufeff ', None)

            table.append(row_dict)

    print("table len: %i, table keys: %s" % (len(table), list(table[0].keys())))
    return table


def read_xml_old(file_name):
    # read xml template to get beginning and ending of the xml file
    # template can be getted in adobe illustrator by adding variables into working space
    file_xml = []
    with open(file_name, encoding='utf-8') as xmlfile:
        file_xml = xmlfile.readlines()

    file_beginning = []
    for string in file_xml:
        if "sampleDataSets" in string:
            # print("True  | " + string)
            file_beginning.append(string)
            break
        else:
            # print("False | " + string)
            file_beginning.append(string)

    file_ending = file_xml[-4:]
    print("file xml len %s and beginning len %s" %(len(file_xml), len(file_beginning)))
    return file_beginning, file_ending



def initialize_xml(keys):
    # read xml template to get beginning and ending of the xml file
    # template can be getted in adobe illustrator by adding variables into working space

    file_beginning = [
        '<?xml version="1.0" encoding="utf-8"?>\n',
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20001102//EN"    ' +
                '"http://www.w3.org/TR/2000/CR-SVG-20001102/DTD/svg-20001102.dtd" [\n',
        '\t<!ENTITY ns_graphs "http://ns.adobe.com/Graphs/1.0/">\n',
        '\t<!ENTITY ns_vars "http://ns.adobe.com/Variables/1.0/">\n',
        '\t<!ENTITY ns_imrep "http://ns.adobe.com/ImageReplacement/1.0/">\n',
        '\t<!ENTITY ns_custom "http://ns.adobe.com/GenericCustomNamespace/1.0/">\n',
        '\t<!ENTITY ns_flows "http://ns.adobe.com/Flows/1.0/">\n',
        '\t<!ENTITY ns_extend "http://ns.adobe.com/Extensibility/1.0/">\n',
        ']>\n',
        '<svg>\n',
        '<variableSets  xmlns="&ns_vars;">\n',
        '\t<variableSet  locked="none" varSetName="binding1">\n',
        '\t\t<variables>\n']

    for key in keys:
        file_beginning += [
            '\t\t\t<variable  category="&ns_flows;" trait="textcontent" varName="' + str(key) + '"></variable>\n'
            ]

    file_beginning += [
        '\t\t</variables>\n',
        '\t\t<v:sampleDataSets  xmlns="&ns_custom;" xmlns:v="&ns_vars;">\n'
    ]

    file_ending = [
        '\t\t</v:sampleDataSets>\n',
        '\t</variableSet>\n',
        '</variableSets>\n',
        '</svg>\n']
    return file_beginning, file_ending


def convert_csv_into_xml_body(table, keys):
    # convert csv file into xml code
    # it provides a possibillity to automatically addition
    # all information into ai file
    file_middle = []
    kk = []
    for i in range(len(table)):
        # if table[i].get('place') != 'участник':
        if True:
            i_key = str(i) + " " + table[i].get(keys[0])
            # i_key = str(i)
            # i_key = table[i].get(keys[0])
            if i_key not in kk:
                kk.append(i_key)
                file_middle.append('\t\t\t<v:sampleDataSet dataSetName="' + i_key + '">\n')
                for key in keys:
                    to_split = ['text', 'task', 'school', 'team', 'status', 'who', 'who2']
                    if (key in to_split) and len(str(table[i].get(key))) > max_len:
                        file_middle.append('\t\t\t\t<' + str(key) + '>\n')
                        str_training_center = str(table[i].get(key))
                        space = str_training_center.rindex(" ", 0, max_len)
                        str_left = str_training_center[space + 1:]
                        str_training_center = [str_training_center[:space]]

                        while len(str_left) > max_len:
                            space = str_left.rindex(" ", 0, max_len)
                            str_training_center.append(str_left[:space])
                            str_left = str_left[space + 1:]
                        str_training_center.append(str_left)

                        for cur_str in str_training_center:
                            file_middle.append('\t\t\t\t\t <p>' + cur_str + '</p>\n')
                        # file_middle.append('\t\t\t\t\t <p>' + str_training_center[1] + '</p>\n')
                        file_middle.append('\t\t\t\t</' + str(key) + '>\n')
                    elif key.lower() == 'name' and split_names:
                        file_middle.append('\t\t\t\t<' + str(key) + '>\n')
                        str_training_center = str(table[i].get(key))
                        space = str_training_center.find(" ")
                        str_training_center_2 = str_training_center[space + 1:]
                        str_training_center = [str_training_center[:space]]


                        space = str_training_center_2.find(" ")
                        str_training_center.append(str_training_center_2[:space])
                        str_training_center.append(str_training_center_2[space + 1:])

                        file_middle.append('\t\t\t\t\t <p>' + str_training_center[0] + '</p>\n')
                        file_middle.append('\t\t\t\t\t <p>'  + str_training_center[1] + " " +  str_training_center[2] + '</p>\n')
                        # file_middle.append('\t\t\t\t\t <p>'  + str_training_center[2] + '</p>\n')
                        file_middle.append('\t\t\t\t</' + str(key) + '>\n')
                    else:
                        file_middle.append('\t\t\t\t<' + str(key) + '>\n')
                        if split_by_n:
                            # values = str(table[i].get(key)).split("\n")
                            values = str(table[i].get(key)).split("\\n")
                            for i_values in values:
                                # file_middle.append('\t\t\t\t\t <p>' + str(i_values) + '</p>\n')
                                # if key.lower() == 'name' or len(str(i_values)) <= max_len:
                                if key.lower() == 'name' or len(str(i_values)) <= t_max_len:
                                    if len(str(i_values)) == 0:
                                        file_middle.append('\t\t\t\t\t <p>' + ' ' + '</p>\n')
                                    else:
                                        file_middle.append('\t\t\t\t\t <p>' + str(i_values) + '</p>\n')
                                else:
                                    str_training_center = str(i_values)
                                    space = str_training_center.rindex(" ", 0, t_max_len)
                                    str_left = str_training_center[space + 1:]
                                    str_training_center = [str_training_center[:space]]

                                    while len(str_left) > t_max_len:
                                        space = str_left.rindex(" ", 0, t_max_len)
                                        str_training_center.append(str_left[:space])
                                        str_left = str_left[space + 1:]
                                    str_training_center.append(str_left)

                                    for cur_str in str_training_center:
                                        file_middle.append('\t\t\t\t\t <p>' + cur_str + '</p>\n')

                        else:
                            file_middle.append('\t\t\t\t\t <p>' + str(table[i].get(key)) + '</p>\n')
                        file_middle.append('\t\t\t\t</' + str(key) + '>\n')

                file_middle.append('\t\t\t' + "</v:sampleDataSet>\n")
            else:
                print(f'Dublicate: {i} || {i_key}')
    return file_middle



if __name__ == '__main__':
    outfile = open(xml_outputname, "w", encoding='utf-8')
    table = read_csv(csv_name)
    table_keys = list(table[0].keys())

    file_beginning, file_ending = initialize_xml(table_keys)
    outfile.writelines(file_beginning)

    file_middle = convert_csv_into_xml_body(table,table_keys)
    outfile.writelines(file_middle)

    outfile.writelines(file_ending)
    outfile.close()
    print("File \"%s\" succsessfully created" % xml_outputname)

